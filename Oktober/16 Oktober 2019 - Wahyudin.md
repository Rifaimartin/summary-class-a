## Ka Safira
Predicate query di tulis setelah table;
**contoh :** `select * from predicate ...`
## Mba Susi
**Example matcher** mempunyai kekurangan, salah satunya tidak bisa **join**. Maka menggunakan criteria builder;
## Bang Billy
Cara mendapatkan satuan dari sebuah `List<>` bisa menggunakan `foreach`;
## Bang Onggo
Berikut merupakan bagian dari method yang ada di **Repository** milik Jpa;
**findAll yang berisi example matcher & specification**
## Ka Elda
Tipe data ada ***Primitive*** & ***Preference***, ketika suatu tipe data ***Primitive*** tidak di inisialisasi hasilnya 0, kalau ***Preference*** hasilnya `null`;
## Rifai
Sebuah interface boleh mengextend 1 atau lebih;
## Juan
**JpaSpecification** bisa mencari atau search sebuah *keyword* dengan **Criteria Builder** dan ***Predicate***;
## Ka Ryan
Cara ***refactor*** dengan metode extract method menggunakan Intellij IDEA**strong text** dengan cara, drag isi dalam method yang mau di ***refactor*** dan klik kanan lalu pilih ***refactor*** dan pilih ***sub extract method***;
## Bang Ragil
Pembuatan class custom **exception** dapat dibuat dengan extend `RuntimeException` dan dapat menggunakan `@RespondStatus` untuk mengtrow status;
## Bang Ilham
Sebuah **transient** ketika di `get` akan menghasilkan `null`, maka dari itu di *getter* **transient** kita isi `if(!(Object == null))` maka `set` **Transient** `Object.getId();`

    getTransient(){
    	if(!(Object == null )){
	    	setTransient(Object.getId);
	    }
    	return transient;
    }
## Mas Deddy
Cara mencari data bisa menggunakan ***Method query***, ***Native query***, ***Example Mathcer***, **Specification**. Dari semua cara ada plus dan minusnya. Seperti **native** akan bisa di **inject**, **Example Matcher** tidak bisa mencari data yang menggunakan **tipe data** `date`;
## Bang Fatiri
**String Utils**, lebih effisien dibanding `String`, **String Utils** ada beberapa fitur yang membantu seperti mengecek sebuah `String` dengan method `isEmpty()`;
## Mang Aldi

> "Sebuah kodingan lebih baik dipahami dibandingkan di hafalkan";
## Ka Winda
Sebuah **table** master harus memiliki 4 atribute wajib yaitu: `createBy`, `modifiedBy`, `createAt`, `updateAt`
## Mike
Mencari bisa menggunakan **specification**, jika search menggunakan **native query** memakai `like` ? `and/or` ...  yaitu satu line tetapi untuk **Specification** `or/and` berada di akhir.
## Mang Bayu
Seperti yang sudah sudah bahwa sebuah method harus di test untuk memastikan kebenaran method tersebut, dan kemarin kita baru saja belajar mengenai **Specification**. Di ambil dari itu bahwa **Specification** dapat di **test**;
## Mas Rifqi
**Operasi aritmatik** dalam `BigDecimal` diantaranya : `add` untuk penjumlahan, `substact` untuk pengurangan, multiply untuk perkalian, `divide` untuk pembagian;
## Adrika
Untuk menggunakan **Specification** Kita harus mengextend **Repository** `JpaSpecificationExecutor`;

<hr/>

> **Kesimpulan** : Bahwa kemarin kita mempelajari cara membuat fitur search dengan berbagai metode. Seperti native query, method query, example matcher dan specification.