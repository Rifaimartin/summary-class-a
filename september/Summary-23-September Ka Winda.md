1. Ketika ingin menampilkan 2 nama field dari 2 tabel yang berbeda,
   menggunakan INNER JOIN.

2. DISTINCT bisa di gunakan untuk menghilangkan duplikasi data dan digunakan
   untuk mengelompokkan data. Perbedaan DISTINCT dengan GROUP BY, DISTINCT tidak
   bisa menggunakan koma(,) di depannya.

3. LEFT JOIN digunakan untuk menampilkan data dari yang sebelah kiri tabel dan 
   menampilkan semua data pada tabel sebelah kiri tapi record di sebelah kanan
   kosong jika tidak ada recordnya.

4. Jika ingin menambahkan colomn baru bisa menggunakan:
   ALTER TABLE namakolom
   ADD COLUMN namakolombaru tipedata;
   Jika ingin menambahkan kolom setelah kolom sks bisa menggunakan AFTER namakolom
   Jika ingin menempatkan kolom baru di urutan pertama bisa menggunakan FIRST

5. Query COUNT digunakan untuk menghitung berapa jumlah data yang ada. 
   contoh query: select count(*) from namatabel ;

6. Penggunaan LIKE dan penempatan (%). Jika tanda (%) di letakkan di depan kata
   maka akan mencari karakter apa saja didepan dan menampilkan kata dibelakangnya
   sesuai dengan kata yang di cari.
   Contoh: SELECT * FROM siswa WHERE nama LIKE '%rifqi' ;

7. Penggunaan UPDATE, digunakan untuk merubah(mengupdate) isi record.
   Contoh: UPDATE namatabel SET namakolom WHERE 

8. Penggunaan DELETE, digunakan untuk menghapus record yang ingin dihapus.
   Contoh : DELETE from namatabel WHERE kondisi
	    DELETE from subject WHERE id = 2;

9. Penggunaan AVERAGE digunakan untuk menghitung rata rata nilai dalam tabel,
   MAX digunakan untuk mencari nilai maximal(tertinggi) dari sebuah record 
   dalam tabel.

10. Mengubah tipe data dalam kolom, menggunakan ALTER
    ALTER TABLE namatabel ALTER COLUMN namakolom tipedata;

11. Penggunaan INTERVAL, digunakan untuk mewakili kurun waktu tertentu.
    Interval <expression> unitwaktu(days,year,month).
    Contoh: ingin mencari tanggal hari kemarin, NOW() - INTERVAL '1 days'

12. Penggunaan SUM, untuk menjumlahkan total nilai satu satu dalam tabel.

13. Tujuan penggunaan INDEX dalam sql untuk meningkatkan performa dalam database
    Contoh: CREATE INDEX

14. Penggunaan ORDER BY, digunakan untuk mengurutkan data. Dalam mengurutkan data
    bisa secara ASCENDING(asc) dan DESCENDING(desc).
    ASCENDING digunakan untuk mengurutkan data dari yang terkecil hingga terbesar.
    DESCENDING digunakan untuk mengurutkan data dari yang terbesar hingga terkecil.
    Contoh : select * from subject ORDER BY sks asc;

15. Cara menggunakan SQLshell postgre:
    1. \l untuk menampilkan database yang telah dibuat
    2. \c nama database -> untuk menggunakan database yang telah dibuat.
    3. \dt -> untuk melihat keselurahan tabel yang ada dalam database.
    4. \d namatabel -> untuk melihat isi tabel(beserta field-fieldnya)

16. Untuk mengetahui total pembayaran yang sudah berangkat yaitu menggunakan
    fungsi sum.

17. Penulisan JOIN, harus ada data yang sama di dalamnya.
    SELECT * field apa yg ingin ditampilkan FROM namatabel1 
    ___ JOIN namatabel2 ON nama field = nama field

18. Penggunaan DISTINCT, untuk menentukan berapa jenis yang ada dalam data.