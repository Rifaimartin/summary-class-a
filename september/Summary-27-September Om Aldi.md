1. Adrika
merubah file-file cfg.xml student dan subject menggunakan anotision yang tandanya menggunakan @.

2. Patiri
mengupdate dan menambahkan otomatis database dengan merubah di hibernate.cfg.xml dengan menambahkan anotasion dan memberikan keterangan kolom pada masing-masing atribut

3. Bayu
untuk menambahkan database di hibernate.cfg.xml dengan menambahkan hibernate.hbm2ddl.auto dengan menambahkan sintaks create

4. Mike
penulisan anotasion berada di atas entity karena pembacaan anotasion itu membaca apa yang ada diBAWAHNYA.

5. Winda
relasi dalam database itu dibagi menjadi ONE TO MANY, MANY TO ONE, dan MANY TO MANY

6. Aldila
fungsi mapped dalam join database adalah sama seperti on pada join table menggunakan SQL, bedanya mapped by menggunakan variabel untuk mencocokan atribut.

7. Ragil
stack over flow terjadi karena 2 buah toString() memanggil toString di kelas masing2. terjadi perputaran pemanggilan sehingga terjadi pemanggilan terus menerus.

8. Onggo
eror di SQLgrammer dikarenakan kesalahan penulisan query, solusinya adalah dengan membuat kolom baru dan melakukan insert secara manual.

9. Elda
RDBMS itu layak di sebut RDBMS jika memenuhi ACID salah satunya Atomicity - kemampuan database untuk memastikan semua bagian transaksi dilakukan atau tidak sama sekali, jika salah satunya gagal maka seluruh transaksi gagal.

10. Juan
For Update kalau ada 2 user yang mau melakukan update data dan melakukan begin. maka akan diantrikan user keduanya setelah user pertama melakukan commit

11. Rifai
dalam menuliskan ID primary key tidak perlu mengenalkan nama kolomnya lagi jadi hanya menggunakan @ID tanpa menggunakan @COLUMN(name="")

12. Bill
anotasion dalam menggunakan relasi dapat menuliskan @ManyToMany, @OneToMany, @ManyToONe dalam hibernate-ORM.

13. Wahyu
JPA adalah sebuah interface native Java J2EE berbasis ORM

14. Agam
Pesimistic locking way - menggunakan SELECT FOR UPDATE, sebelum 2 user berbarengan akan diantrikan sebelum melakukan commit dari user ke 1 
Optimistic locking way - kedua user tetap bisa mengakses table tetapi user ke2 mendapatkan table yang telah di commit oleh user pertama. tambahan dari kak Arif = solusinya dapat menggunakan multi Thread

15. Ilham
didalam hibernate.cfg.xml jangan sampai tertukar mappingnya, di file itu terdapat property-property seperti untuk menampilkan sqlnya menggunakan property show_sql dan ubah menjadi true.

16. Rifqi
untuk menyimpan ke dalam database dalam hibernate menggunakan save. dengan kepintaran hibernate, fungsi save itu dapat menyimpan atau juga dapat mengupdate.