## 18/09/2019

1. Abstrac => digunakan hanya untuk membuat sebuah method yang tanpa ada implementasinya
2. Set	=> mengurutkan data dari yang terkecil sampai yang terbesar
3. Collection 	=> ada interface dan class
		- ```Dalam interface ada set, list, queue, dequeue, map```
		- ```Dalam class ada arraylist, vector, likendlist, hashet```
4. Collection => sebuah framwork, yang bisa menyimpan dan memanipulasi sebuah object
5. penggunaan IDE tidak harus ketik ulang, untuk menggunakan geter seter karena menggunakan alt+insert
6. equals	=> untuk menbandingkan valuenya
7. abstraction 	=> interface => sifat
		=> abstraction => bentuk, interface yang diturunkan
		=> jika abstarac implement interface, abstrac tidak terpengaruh tetapi class turunannya yang terpengaruh (Elda)
8. interface => bentuk dari polymerpism yang berbentuk sifatnya
9. list => pengelompokan secara urut
10. fungsi geter seter	=> geter untuk mengambil data
			=> seter untuk menampilkan data
			