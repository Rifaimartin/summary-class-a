Summary 19 September 2019
Notulen : Elda Oktaviani
---------------------------------------------------
Trowable :
1. Error : berkaitan dengan native
2. Exception : Runtime Exception, IOException Exception 
Error dan Exception dihandle oleh IDEA.

- Split(regex) : memisahkan dan sesuaikan pemisah dengan apa yg kita inginkan
- Trim : menghilangkan spasi yg ada dikanan dan kiri
- Substring :menghilangkan huruf dari index yg kita inginkan, butuh 2 index, diitung pointernya, awal dan akhir, mengambil part of string dari index yang dibutuhkan

Aplikasi disimpan di RAM & CPU dan bersifat volatile(membutuhkan daya) maka jika daya mati maka akan di clean

File Service : 
1. Read : Membaca file yang ada di dalam hardisk
2. write : Menuliskan sesuatu kedalam hardisk, bentuk yang dapat disimpan di hardisk adalah file, file punya ekstension bisa berekstensi .txt atau format lainnya

- FOS (File Output Stream) : aliran output yang digunakan untuk menulis data ke file, cuma bisa kode asci, ukurannya byte
- fileWriter : digunakan untuk menulis data yang berorientasi karakter ke sebuah file, isinya bisa string.
fileWrite.write : menulis dan menimpa text
fileWrite.append : menambah text
fileWrite.close : untuk nutup kalo write text
- bufferedwriter : menyediakan buffering untuk writer instance, isinya bisa string.
buffered.newLine : bisa langsung enter

- FIS (File Input Stream: membaca data dari file dalam bentuk urutan byte
- file reader : digunakan untuk mencari dan membaca data dari file, baca karakter per karakter
FIleReader.read = membaca karakter perkursor
- BufferedReader : class untuk membaca text dari inputan karakter, bisa membaca karakter per line

printStackTree : nyetak dirinya dan exception. kalo dia berhasil nangkep, nyuruh dia untuk menyetak dirinya sendiri.

ngehandle nullpointerexception ? 
1.trows : dilempar keluar atau ke yang manggil fungsi dibikin, tidak bisa dipake dikelas hanya bisa dipake di method.
2.trycatch : menentukan tanggung jawabnya untuk menangkap error

penulisan if ada satu macam lagi
1. ternary logic condition, penulisannya a==8?"benar":"salah";
cara bacanya jika kondisi a = 8 maka "benar" jika a=7 maka "salah"
hasil dari if ini adalah string dan harus ditampung kedalam variable.

```java
    variable_name = (condition) ? value_if_true : value_if false;
```

Contoh

```java
     // absolute value example
    a = -10;
    int absValue = (a < 0) ? -a : a;
    System.out.println("abs = " + absValue);
```
Nesting Ternary Operator

```java
    String msg = num > 10 ? "Number is greater than 10" : 
    num > 5 ? "Number is greater than 5" : "Number is less than equal to 5";

    //or 
    String msg = num > 10 ? "Number is greater than 10"
    : (num > 5 ? "Number is greater than 5" : "Number is less than equal to 5");

```

- ```errorNullException``` : error yg belum terinisialisasi, itu keluar kalo udah diapa-apain.

- ```array indexOutOfBounce``` : errornya karena arraynya yg diprint gak sesuai dengan yg diinput/saat kita mau mengakses element yg tidak ada didalam itu.

- map : menggunakan key value
- hashset : pake hashcode
- list : pake equals karena kalo gak dipanggil bukan data yg sebenernya tapi malah alamat memorinya
- tujuan ```.equals``` : array list menggunakan equals untuk membandingkan alamat elementnya
- kode asci : ```kode yg menerima inputan karakter```

- MESIN/komputer proses didalamnya pake biner 1 0
untuk menampilkan karakter asci code contoh 65
